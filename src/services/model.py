from catboost import CatBoostClassifier
import joblib

from typing import List 

class EmotionClassifier:
    """
    Классификатор эмоций на основе предобученной модели Transformers.

    Methods:
        predict_emotion: Предсказание эмоций по тексту.
    """

    loaded_model = CatBoostClassifier()
    loaded_model.load_model('src/data/model/model.cbm')
    vectorizer = joblib.load('src/data/vectorizer/vectorizer.joblib')
    
    @classmethod
    def predict_emotion(cls, text: str) -> List[str]:
        """
        Предсказание эмоций по тексту.

        Args:
            text (str): Текст для анализа эмоций.

        Returns:
            dict: Результат предсказания в формате словаря.
        """
        
        text_processed = cls.vectorizer.transform([text])
        result = cls.loaded_model.predict(text_processed)
        
        print('++++++++++++++++++++++++++++++++++++++')
        print(result)
        print('++++++++++++++++++++++++++++++++++++++')
        
        return result
    

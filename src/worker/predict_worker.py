from loguru import logger

from src.connections.broker import celery_app 
from src.services.model import EmotionClassifier


@celery_app.task(name='predict', serializer='json')
def predict_emotion(text: str):
    """
    Предсказание эмоции по тексту

    Args:
        text (str): текст, "настроение" которого необхдимо определить
    """
    
    try:
        result = EmotionClassifier.predict_emotion(text)
        return result.tolist()
    except Exception as ex:
        logger.error([ex])
        return None
    
# poetry run celery -A src.worker.predict_worker worker --loglevel=info 
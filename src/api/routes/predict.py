from fastapi import APIRouter, BackgroundTasks
from src.schemas.requests import TextRequest
from src.services.utils import print_logger_info
from src.connections.broker import celery_app

router = APIRouter()

@router.post("/predict/")
async def predict_emotion(text_request: TextRequest, background_tasks: BackgroundTasks):
    """
    Предсказание эмоции по тексту.

    Args:
        text_request (TextRequest): Текстовый запрос.

    Returns:
        dict: Результат предсказания в формате словаря.
    """
    
    print('====================================================================')
    print(text_request.text)
    print('====================================================================') 
    
    async_result = celery_app.send_task('predict', args=[text_request.text])
    result = async_result.get()
    background_tasks.add_task(
        print_logger_info,
        text_request.text,
        result,
    )
    
    return result[0][0]
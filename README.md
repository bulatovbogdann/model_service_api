# Model Service Api

Example The ML service is a web application that provides an API for interacting with a machine learning model. It allows users to send queries with prediction data and get results back.

**Startup logic:**

When launched, the application initializes FastAPI, which handles HTTP requests. The app also connects to the machine learning model and loads it into memory for use in making predictions.

```
│   .dockerignore
│   .gitignore
│   docker-compose.yml    # Файл для управления контейнерами Docker
│   poetry.lock
│   pyproject.toml
│   README.md
│   tasks.py
├───.docker
│       api.Dockerfile           # Файл Dockerfile для создания образа контейнера
│       worker.Dockerfile        # Файл Dockerfile для создания образа воркера для Celery
└───src
    │   app.py
    ├───api
    │   │   __init__.py
    │   └───routes
    │           healthcheck.py
    │           predict.py
    │           router.py
    │           __init__.py
    ├───connections
    │      broker.py
    ├───data
    │   │   data.csv              # данные для тренировки модели и векторайзера
    │   ├───model
    │   │       model.cbm         # модель
    │   └───vectorizer
    │           vectorizer.joblib # векторайзер
    ├───schemas
    │       healthcheck.py
    │       requests.py
    │       __init__.py
    ├───services
    │      model.py
    │      utils.py
    │      __init__.py
    └───worker
          predict_worker.py

```

## Getting started

```
docker-compose up --build
```
web-server on
```
http://0.0.0.0:7007
```
swagger ui on
```
http://0.0.0.0:7007/docs
```
For test
```
curl -X 'POST' \
  'http://0.0.0.0:7007/api/predict/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "text": "Привет, как дела? Что нового?"
}'
```